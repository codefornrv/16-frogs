# 16 Frogs in Blacksburg

## About this project

This project is a public website for the 16 Frogs public art installation in Blacksburg, VA.

It can be found at https://16frogs.org
