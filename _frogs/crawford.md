---
name: Crawford
shortname: crawford
order: 12
blurb: I’m named for the Crawford family, who ran a general store near here in the mid-1900s. This area was mostly farmland then. The Webb Branch of Stroubles used to flood the basement of the store!
location: Webb St. near Prices Fork Rd.
layout: frog
---

Near the present-day corner of Turner Street and Prices Fork, a general store served the growing population of Blacksburg in the mid-1900s. Known over the years as Spangler's, Cutherell's, and the Branch store—after the Webb branch of Stroubles, which ran next to and sometimes even through the building—the shop is perhaps best remembered as Crawford’s, for the family that ran it in the 1940s and 1950s.

Roy Crawford bought the store from its previous owner, Mr. Spangler, on a whim one afternoon. He’d stopped in to shop, but when Spangler complained about business during wartime—the ration stamps, the dwindling labor pool, the shortage of merchandise for civilians—Crawford took over with a handshake. Open from 7 am to 6 pm every day except Wednesday afternoons and Sundays, the store sold cards, china, decorations, clothing, and, at Christmastime, toys. The Crawfords also offered food, including Lusters Gate hoop cheese, and pickles that were fished out of wooden barrels using a wood plank with a nail for a hook. Roy’s wife, Betty Ann, ran a beauty shop with a separate entrance at the back of the store.

In the 1940s, this was the outskirts of town. Prices Fork Road did not yet connect to Main Street; it ended at the intersection with Turner Street—then called Pepper Street. Aside from about a dozen families living along Pepper Street, the area was rural. Vacant fields dotted the neighborhood, and what would become McBryde Village was still farmland. Nevertheless, the store served as a gathering place for the residents of Blacksburg, which even then was home to people from around the world. Doctors, lawyers, drummers, students, teachers, coal miners, construction workers, those that never worked and those that always worked, any kid who had a penny: all were welcome. In hard times, the Crawfords let groceries be charged. Some accounts weren’t settled for years; some never were. The Crawfords had lived through the depression and could not allow anyone to go hungry.

The Webb branch of Stroubles Creek runs above ground here on its course from North Main Street to the Duck Pond. In rainy months, it invaded the basement of the store, followed inevitably by wetland muskrats. A spring behind the house was filled with watercress, which the locals harvested. 

Take action! If you spill automotive fluids or household chemicals, use cat litter or a proper absorbent to contain it. Then collect the debris with a broom or shop-vac and dispose in the trash. Hosing with water will channel the waste into the storm-drain system and ultimately into the stream.
